function [] = Affichage(x, y, a, b, approx, fctCout, nom)
  aApprox = approx(1, :);
  bApprox = approx(2, :);

  figure;
  contour(a, b, fctCout, 20); title('Repr�sentation 2D de la fonction de co�t robuste'); xlabel('b'); ylabel('a');
  hold on;
  plot(bApprox, aApprox, '-r');
  hold off;
  legend({'Fonction de co�t', nom});

  figure;
  subplot(2,2,1);
  distance = CalculDistance(approx);
  plot(distance); 
  xlabel('It�ration'); ylabel('Distance'); 
  title('Distance en fonction de l''it�ration');
  axis auto; 
  
  subplot(2,2,2);
  [cout, normeGrad] = EvolutionFctCoutEtGrad(aApprox, bApprox, x, y);
  plot(cout);
  hold on;
  plot(normeGrad);
  hold off;
  xlabel('Nombre d''iterations');
  axis auto;
  title('�volution de la fonction co�t et de la norme du gradient');
  legend({'Fonction co�t', 'Norme du gradient'});

  subplot(2,2,3);
  totalDistances = CalculDistanceTotale(distance);
  plot(totalDistances); 
  xlabel('Nombre d''iterations'); ylabel('Distance totale'); 
  title('Distance totale en fonction du nombre d''it�rations');
  axis auto;
end
