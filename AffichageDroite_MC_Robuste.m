function [] = AffichageDroite_MC_Robuste(x,y,aInit,bInit,NMax,epsilon,sigma)
if nargin<7
    sigma = 1;
end
approxPenteMC = PlusFortePenteMC(epsilon,NMax,x,y,aInit,bInit);
approxPenteRobuste = PlusFortePenteRobuste(epsilon,NMax,x,y,aInit,bInit,sigma);

a_MC = approxPenteMC(1,end);
b_MC = approxPenteMC(2,end);

a_rob = approxPenteRobuste(1,end);
b_rob = approxPenteRobuste(2,end);

figure
plot(x,y,'+'),xlabel('x'),ylabel('y')
hold on
fplot(@(x) a_MC*x+b_MC);
hold off
hold on
fplot(@(x) a_rob*x+b_rob);
hold off
legend('Points','Moindres carrés','Robuste');
title(strcat('Représentation droites ajustées, a0 = ',int2str(aInit),', b0 = ',int2str(bInit),', sigma =',int2str(sigma)))
end