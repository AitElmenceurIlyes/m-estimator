function [cout, normeGrad] = EvolutionFctCoutEtGrad(a, b, x, y)
  N = size(a, 2);
  cout = [];
  normeGrad = [];
  for i=1:N
    % On r�cup�re toutes les p�nalisation de cauchy pour les diff�rents points 
    % (a b)' pour voir l'�volution.
    cout(end + 1) = FonctionCoutRobuste(1, a(i), b(i), x, y);
    
    % De m�me, on r�cup�re la norme de chaque gradient en tout point (a b)'.
    aGradient = AGradient(1, a(i), b(i), x, y);
    bGradient = BGradient(1, a(i), b(i), x, y);
    normeGrad(end + 1) = sqrt(aGradient^2 + bGradient^2);
  end
end