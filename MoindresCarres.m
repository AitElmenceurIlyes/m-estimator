function [ m ] = MoindresCarres( a,b,x,y )
% MoindresCarres Summary of this function goes here
% Detailed explanation goes here

taille = size(x,1);

if taille ~= length(y)
    error('Les tailles de x et y sont différentes');
end

m = 0;
    for i=1:taille
        m = m + (a*x(i) + b - y(i))^2;
    end
end

