# Projet d’optimisation continue

## Estimation robuste: les M-estimateurs

Le but de ce projet est d’appliquer les méthodes d’optimisation continue à un cas concret : le problème de l’estimation robuste, c’est-à-dire l’estimation en présence de points aberrants (outliers). Nous nous intéressons à une classe d’estimateurs appelés M-estimateurs qui sont définis sous la forme d’un problème d’optimisation continue.

### Description du problème

On a réalisé *n* points de mesure *(x1, y1)*, *(x2, y2)*, . . . *(xn, yn)*. On souhaiterait ajuster une droite *y = a x + b* aux points mesurés. Cependant, la présence de points aberrants rend assez mauvais l’ajustement au sens des moindres carrés. On préfère alors utiliser d’autres estimateurs, robustes aux mesures aberrantes : les M-estimateurs.

