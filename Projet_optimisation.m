%% MOUTAA AIT ELMENCEUR
close all
clear
data=open('data.mat');
x = data.x;
y = data.y_noisy;
%% 2.Estimation au sens des moindres carr�s
%% Q1
% On a sur la figure 1, a environ 2 et b environ 0. On va choisir un espace
% de valeur (a,b).
a = 0:0.1:10;
b = -8:0.1:2;
C_mc=zeros(length(a),length(b));
tic
for i=1:length(a)
    for j=1:length(b)
        C_mc(i,j)=MoindresCarres(a(i),b(j),x,y);
    end
end
toc
min_mc1=min(min(C_mc)); %avec pas de 0.1

figure,mesh(b,a, C_mc)
title('Fonction des moindres carr�s, pas de 0.1')
xlabel('b'),ylabel('a')
%% Q2
% Echantillonnage r�gulier des param�tres a et b :

N = 200;
a=linspace(-10,10,N); % pas de 0.2
b=linspace(-10,10,N);

C_mc=zeros(N);

tic
for i=1:N
    for j=1:N
        C_mc(i,j)=MoindresCarres(a(i),b(j),x,y);
    end
end
toc

ApproxIterative = MinMatrice(C_mc,[-10 10],N);

figure,contour(b,a,C_mc)
hold on
plot(ApproxIterative(2),ApproxIterative(1),'g*')
hold off
title('Fonction des moindres carr�s, pas de 0.01')
xlabel('b'),ylabel('a')
%% Q3
% On calcule le gradient directement sur la matrice (avec les normes), on
% obtient une �galit� pour la solution z = [a;b],

A = [x,ones(30,1)];
B = y;
format long
z = (A.'*B) / norm(A,2)^2;
%% z = round(z,6,'significant');
z = round(z);
% 5.46723;1.69726
%% Q4
hold on
plot(z(2),z(1),'r*')
legend('Moindres Carr�s','Approx par �chantillonnage r�gulier','Approx moindres carr�s')
hold off

%% Q5
figure
iterative = @(x) ApproxIterative(1)*x+ApproxIterative(2);
approx = @(x) z(1)*x+z(2);
fplot(iterative,[-2 5]);
hold on
fplot(approx,[-2 5]);
hold off
hold on
plot(x,y,'+')
hold off

title("Repr�sentation des points de mesure et de la droite d'approximation")
legend('iterative','moindres carr�s','data')
xlabel('x'),ylabel('y')
% C'est un estimateur peu pr�cis du fait des valeurs aberrantes

%% 3.Estimation robuste

%% Q6
% Configuration initiale : Gradient de f, 1ere approx de ab0, pr�cision
% demand�e epsilon.
A = [x,ones(30,1)];
B1 = y;
%1ere approx de ab0
ab0=[5 2];
%Calcul de f(ab0) et -grad_f(ab0) = plus forte pente
MC = MC_Mat(x,y,ab0);
% Agrad : sigma, a,b x,y
Grad_MC = GradMC_Mat(x,y,ab0);
d = - Grad_MC;

% Calcul du pas alp0
% Algorithme de Fletcher-Lemar�chal
% Config init : f et grad de f, ab0, direction de descente d, approx alp0,
% beta1, beta2 et lambda>1,/ init i=0, alpl=0, alpr=+inf;
alp0=1e-3;
beta1 = 1e-3;
beta2 = 0.99;
lambda = 20;
alpl=0;
alpr=100000;

alpha = FletcherLeMarechal(ab0,d,10,Grad_MC,x,y ,alp0, alpl, alpr, beta1, beta2, lambda);
X = PlusFortePenteMC(1e-3,10000,x,y,ab0(1),ab0(2));

%Repr�sentation du gradient de moindre carr�s
% Le gradient pointe dans 2 directions diff�rentes dans la vall�e, la
% progession de la m�thode des plus fortes pentes est lente.
aGradient = zeros(N);
bGradient = aGradient;
for i=1:N
  for j=1:N
    grad = GradMC_Mat(x,y,[a(i),b(j)]);
    aGradient(i, j) = grad(1);
    bGradient(i, j) = grad(2);
  end
end
TablX = zeros(1, N^2);
TablY = TablX;
for i=1:N
  TablX(N*(i-1)+1:N*i) = linspace(a(i), a(i), N);
  TablY(N*(i-1)+1:N*i) = a;
end
AGrad = reshape(aGradient, [1, N^2]);
BGrad = reshape(bGradient, [1, N^2]);

figure,contour(b,a,C_mc)
xlabel('b'),ylabel('a')

hold on;
quiver(TablX, TablY, AGrad, BGrad);
axis equal;
hold off;

hold on;
plot(X(2,:),X(1,:),'-r')
hold off;
% Dernier point
xAppr=[X(1,end);X(2,end)];
hold on
plot(xAppr(2),xAppr(1),'+r')
hold off
%% Q7

p = @(r) (1/2)*log(1+r.^2);
pPrime = @(r) r/(1+r.^2);
pSeconde = @(r) (1-(r.^2))/(1+(r.^2)^2);

figure
fplot(p, [-10 10]);
hold on
fplot(pPrime,[-10 10]);
hold on
fplot(pSeconde, [-10 10]);
hold off
xlabel('r'); ylabel('p');
title('Repr�sentation de la p�nalisation de Cauchy pour sigma = 1');
legend({'Fonction de p�nalisation','D�riv�e premi�re', 'D�riv�e seconde'});

% La d�riv�e seconde n'est pas positive sur tout le domaine de d�finition,
% la fonction de p�nalisation n'est donc pas convexe.
%% Q8
dim = 200;
bornes = [-5,5];
a = linspace(bornes(1),bornes(2),dim);
b = a;
MatriceFctCout = zeros(dim);

for i = 1:dim
    for j= 1:dim
        MatriceFctCout(i,j)=FonctionCoutRobuste(1,a(i),b(j),x,y);
    end
end

point = MinMatrice(MatriceFctCout,bornes,dim);

figure(6);
contour(a, b, MatriceFctCout, 40); xlabel('a'); ylabel('b');
legend({'Fonction co�t'});
title('Mod�lisation de l''estimation robuste');

%% Q9
% On d�finit le gradient en 2 fct, d�riv�es partielles de la fct
dbtype('AGradient.m')
aGradient = zeros(dim);
bGradient = aGradient;

for i=1:dim
  for j=1:dim
    aGradient(i, j) = AGradient(1, a(i), b(j), x, y);
    bGradient(i, j) = BGradient(1, a(i), b(j), x, y);
  end
end

%  On cr�e deux vecteurs qui vont permettrent de servir de rep�re � la fonction
%  quiver. Ils vont balayer tous les points de l'espace en y r�f�rencant le
%  gradient correspondant.
TablX = zeros(1, dim^2);
TablY = TablX;

for i=1:dim
  TablX(dim*(i-1)+1:dim*i) = linspace(a(i), a(i), dim);
  TablY(dim*(i-1)+1:dim*i) = a;
end

% On met les matrices gradient � plat sous forme de vecteur.
AGrad = reshape(aGradient, [1, dim^2]);
BGrad = reshape(bGradient, [1, dim^2]);

hold on;
quiver(TablX, TablY, AGrad, BGrad);
axis equal;
title('Gradient (a b) de la fonction co�t');
hold off;
% Traits blancs : minima locaux (zones o� les vecteurs sont de norme
% faible)
% On rep�re le min global vers la zone centrifuge
% Champs vectoriel,

%% Q10

NMax = 2000;
epsilon = 10^-4;
% Point de d�part : 5,5
approxPlusFortePente1 = PlusFortePenteRobuste(epsilon, NMax, x, y, 5, 5);
Affichage(x, y, a, b, approxPlusFortePente1, MatriceFctCout, 'Plus forte pente d�part [5;5]');

%% Q11

% On change les points de d�part
approxPlusFortePente2 = PlusFortePenteRobuste(epsilon, NMax, x, y, 2, 5);
Affichage(x, y, a, b, approxPlusFortePente2, MatriceFctCout, 'Plus forte pente d�part [2;5]');

approxPlusFortePente3 = PlusFortePenteRobuste(epsilon, NMax, x, y, 5, 2);
Affichage(x, y, a, b, approxPlusFortePente3, MatriceFctCout, 'Plus forte pente d�part [5;2]');

% La fonction converge lentement lorsqu'on est dans la vall�e, en effet
% dans cette zone le gradient est faible, d'o� une pente faible et des
% petites distances parcourues.

%% Q12

approxNewton1 = QuasiNewton(x, y, 5, 5,epsilon, NMax);
quasiNewton1 = [approxNewton1(1, end); approxNewton1(2, end)];
Affichage(x, y, a, b, approxNewton1, MatriceFctCout, 'Quasi-Newton, d�part [5;5]');

% La m�thode de quasi Newton, contrairement � la m�thode des plus fortes
% pentes, reste dans des zones o� le gradient est important afin de
% converger plus vite. Cependant elle peut s'�loigner temporairement de la
% solution, l� o� l'autre m�thode s'approche constamment, avec des pas tr�s
% faibles.

%% Q13
NMax = 1000;
epsilon = 1e-3;
% Pour diff�rentes valeurs initiales de a et b
AffichageDroite_MC_Robuste(x, y, 5, 5, NMax, epsilon, 1);
AffichageDroite_MC_Robuste(x, y, 2, 5, NMax, epsilon, 1);
AffichageDroite_MC_Robuste(x, y, 5, 2, NMax, epsilon, 1);


% Variations de sigma
AffichageDroite_MC_Robuste(x, y, 5, 5, NMax, epsilon, 1);
AffichageDroite_MC_Robuste(x, y, 5, 5, NMax, epsilon, 100);
AffichageDroite_MC_Robuste(x, y, 5, 5, NMax, epsilon, 1000000);
% Si sigma est tr�s grand, on n�glige le r�sidu et les approximations de a
% et b ne diff�rent pas de a et b initiaux


