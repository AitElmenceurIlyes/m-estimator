function [approx] = QuasiNewton(x, y, a0, b0, epsilon, NMax, sigma)
  % Initialisation
  if nargin<7
      sigma = 1;
  end
  % Cf poly ch3 diapo 29
  % Fonction f : p�nalisation de cauchy, gradient connu
  % 1ere approximation a0, b0
  point = [a0; b0];
  % - Matrice identit� 2x2.
  I = eye(2);
  % - H initiale est la matrice identit�
  H = I;
  
  % Calcul du gradient : 
  aGrad = AGradient(sigma, a0, b0, x, y);
  bGrad = BGradient(sigma, a0, b0, x, y);
  
  % - Initialisation du tableau retourn� qui contient l'ensemble des points 
  % (a; b) sous forme de vecteur 2x1.
  approx = [point];
  
  % - Calcul de la norme initiale
  norme = sqrt(aGrad^2 + bGrad^2);
  
  k = 0;
  
  while (k < NMax && norme > epsilon)    
    % Stockage du gradient dans une matrice 2x1
    grad = [aGrad; bGrad];
    % Calcul de la direction, matrice 2x1
    d = -H*grad;
    % D�termination de Alpha_K avec FL
    alpha = FletcherLeMarechal(point, d, 10, grad, x, y, 1, 0, 10^6, 10^-3, 0.99, 20);
    % Calcul du nouveau point.
    point = point + alpha * d;
    k=k+1;
    
    % Mise � jour de H :
    % Calcul du nouveau gradient.
    aGrad = AGradient(sigma, point(1), point(2), x, y);
    bGrad = BGradient(sigma, point(1), point(2), x, y);
    % Calcul de Y_k-1.
    yk = [aGrad; bGrad] - grad;
    % Calcul de d_k-1 barre.
    dbar = alpha*d;
    % On d�coupe le calcul des 3 matrices dans le calcul de H
    MatriceGauche = I-((dbar*yk')/(dbar'*yk));
    MatriceDroite = I-((yk*dbar')/(dbar'*yk));
    Addition = (dbar*dbar')/(dbar'*yk);
    % Nouveau H
    H = (MatriceGauche*H*MatriceDroite)+Addition;
    
    % Calcul de la nouvelle norme.
    norme = sqrt(aGrad^2 + bGrad^2);
    % On ajoute le point dans la liste
    approx = [approx, point];
  end
end