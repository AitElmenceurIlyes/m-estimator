function [p] = FonctionCoutRobuste(sigma, a, b, x, y)
% Calcul de la fonction de coût robuste ; somme de 1 à N de la pénalisation
% de Cauchy.
  % Initialisation
  p = 0;
  N = size(x,1);

  for i=1:N
    % Calcul du r actuel.
    ri = a * x(i) + b - y(i);
    % Calcul de p et ajout au dernier p.
    p = p + (1/2)*log(1 + (ri/sigma)^2);
  end
end