function [point] = MinMatrice(matrice, bornes, dimension)
  [M,I] = min(matrice);
  [~, column] = min(M);
  row = I(column);
  scale = (bornes(2)-bornes(1))/dimension;
  aMin = bornes(1) + row * scale;
  bMin = bornes(1) + column * scale;
  point = [aMin; bMin];
end
