function [ gradmc ] = GradMC_Mat( x,y,v)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% v vecteur ligne avec les valeurs de a et b
A = [x,ones(30,1)];
gradmc = 2 * A.' * ( A * v.' - y);
end

