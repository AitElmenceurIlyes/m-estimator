function [DistanceTotale] = CalculDistanceTotale(MatriceDistances)
  % La distance s'ajoute � chaque it�ration, distance totale strictement
  % croissante
  N = size(MatriceDistances, 2);
  DistanceTotale = zeros(1,N);
  for i=1:N
    % On ajoute les distances au fur et � mesure
    DistanceTotale(i + 1) = DistanceTotale(i) + MatriceDistances(i);
  end
end