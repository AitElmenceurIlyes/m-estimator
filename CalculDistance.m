function [distance] = CalculDistance(approx)
  % On pose a et b pour simplifier le calcule de la norme
  a = approx(1, :);
  b = approx(2, :);
  % Taille maximale pour conna�tre le nombre d'it�ration � effectuer.
  N = size(a, 2);
  distance = zeros(1,N-1);
  
  for i=1:N-1
    % On calcule la distance entre chaque point
    distance(i) = sqrt((a(i+1) - a(i))^2 + (b(i+1) - b(i))^2);
  end
end
