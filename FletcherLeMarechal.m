function [alpha] = FletcherLeMarechal(point, d, NMax, grad, x, y, alpha0, alphaL, alphaR, beta1, beta2, lambda)
  
  % Initialisation
  i = 1;
  arret = false;
  alpha = alpha0;
  infini = alphaR;
  
  while(i <= NMax && arret == false)
    % Calcul de gamma
    gamma = -beta1*grad'*d;
    % Calcul cauchy au point initial
    CauchyX = FonctionCoutRobuste(1, point(1), point(2), x, y);
    % Point correspond au point initial auquel on ajoute alpha * direction
    point = point + alpha * d;
    % Cauchy au point x+ad.
    CauchyXPlusAlphaD = FonctionCoutRobuste(1, point(1), point(2), x, y);
    
    %% Vérification de la condition CW1
    if(CauchyXPlusAlphaD <= CauchyX - alpha * gamma)
      CW1 = true;
    else 
      CW1 = false;
    end
    
    % Calcul du gradient au point x+alpha*d pour vérifier CW2
    gradientOnXPlusAlphaD = [AGradient(1, point(1), point(2), x, y); BGradient(1, point(1), point(2), x, y)];
 
    % Calculons la dégénérescence.
    deg = (gradientOnXPlusAlphaD'*d)/(grad'*d);
    
    % Vérif CW2
    if(deg <= beta2)
      CW2 = true;
    else
      CW2 = false;
    end
    
    % Appliquons les changements si les deux conditions CW1 et CW2 ne sont pas 
    % respectées. On applique les nouvelles valeurs de alpha si une des deux 
    % conditions au moins n'est pas remplie. 
    if (CW1 == false || CW2 == false)
      if (CW1 == false)
        % Pas trop long
        alphaR = alpha;
        alpha = (alphaL + alphaR)/2;
      else 
        % Pas trop court
        alphaL = alpha;
        % Infini est la valeur initiale de AlphaR.
        if (alphaR < infini)
          alpha = (alphaL + alphaR)/2;
        else 
          alpha = alpha * lambda;
        end
      end
    else
      arret = true;
    end 
    i=i+1;
  end
end